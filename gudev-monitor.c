// gcc gudev-monitor.c -o gudev-monitor `pkg-config --cflags --libs gudev-1.0 glib-2.0`

#include <gudev/gudev.h>
#include <gio/gio.h>

static void
print_device (GUdevDevice *device)
{
  const char * const *keys = g_udev_device_get_property_keys (device);
  const char * const *sysfs_attrs = g_udev_device_get_sysfs_attr_keys (device);
  
  if (!g_udev_device_get_device_file (device))
    return;
  
  g_print ("\n");
  if (g_udev_device_get_action (device))
    g_print ("[%s] %s\n", g_udev_device_get_action (device), g_udev_device_get_sysfs_path (device));
  else
    g_print ("%s\n", g_udev_device_get_sysfs_path (device));
  g_print ("============================\n");
  g_print ("    driver: %s\n", g_udev_device_get_driver (device));
  g_print ("    subsystem: %s\n", g_udev_device_get_subsystem (device));
  g_print ("    device file: %s\n", g_udev_device_get_device_file (device));
  g_print ("    initialized: %d\n", g_udev_device_get_is_initialized (device));
  
  g_print ("    properties:\n");
  for (size_t i = 0; keys && keys[i]; i++)
    g_print ("        %s: %s\n", keys[i], g_udev_device_get_property (device, keys[i]));

  g_print ("    sysfs attrs:\n");
  for (size_t i = 0; sysfs_attrs && sysfs_attrs[i]; i++)
    g_print ("        %s: %s\n", sysfs_attrs[i], g_udev_device_get_sysfs_attr (device, sysfs_attrs[i]));
  
  g_print ("============================\n");
}

static void
on_gudev_client_uevent_cb (GUdevClient *client,
                           const char  *action,
                           GUdevDevice *device,
                           gpointer user_data)
{
  print_device (device);
}

int main(int argc, char *argv[])
{
  g_autoptr(GPtrArray) udev_subsystems = NULL;
  g_autoptr(GMainLoop) mainloop = NULL;
  GUdevClient *client;

  mainloop = g_main_loop_new (NULL, FALSE);
  
  udev_subsystems = g_ptr_array_new_full (2, g_free);
  for (int i = 0; i < argc - 1; i++)
    g_ptr_array_add (udev_subsystems, g_strdup (argv[i + 1]));
  g_ptr_array_add (udev_subsystems, NULL);
  
  client = g_udev_client_new ((const char * const *) udev_subsystems->pdata);
  g_signal_connect (client,
                    "uevent",
                    G_CALLBACK (on_gudev_client_uevent_cb),
                    NULL);

  if (argc > 1)
    {
      for (int i = 0; i < argc - 1; i++)
        {
          g_autolist(GUdevDevice) devices = NULL;

          devices = g_udev_client_query_by_subsystem (client, argv[i + 1]);
          for (GList *l = devices; l; l = l->next)
            print_device (l->data);
        }
    }
  else
    {
      g_autolist(GUdevDevice) devices = NULL;

      devices = g_udev_client_query_by_subsystem (client, NULL);
      for (GList *l = devices; l; l = l->next)
        print_device (l->data);
    }
  
  g_main_loop_run (mainloop);
  
  return 0;
}
         
